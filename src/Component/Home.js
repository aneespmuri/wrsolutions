import React, {useState} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import DropDownPicker from 'react-native-dropdown-picker';
import {BarChart, Grid} from 'react-native-svg-charts';

const App = ({navigation}) => {
  const [country, setcontry] = useState('uk');
  const [data, setdata] = useState([10, 20, 30, 40]);

  const handleData = (item) => {
    console.log(item.value);

    if (item.value === 'uk') {
      setdata([18, 9, 2, 4, 5]);
    } else {
      setdata([10, 20, 30, 40]);
    }
  };
  const fill = 'rgba(1, 101, 177, 0.5)';

  return (
    <View style={{backgroundColor: '#0091FF', flex: 1}}>
      <ScrollView>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            position: 'relative',
          }}>
          <TouchableOpacity
            onPress={() => navigation.openDrawer()}
            style={{position: 'absolute', left: 25, zIndex: 10}}>
            <Image
              style={{width: 30, height: 30}}
              source={require('../Asset/img/open.png')}
            />
          </TouchableOpacity>
          <Image
            style={{width: 174, height: 100}}
            resizeMode={'center'}
            source={require('../Asset/img/WRSolutions.png')}
          />
        </View>
        <View
          style={{
            backgroundColor: '#FBFBFB',
            flex: 1,
            paddingHorizontal: 16,
            paddingVertical: 40,
            borderTopLeftRadius: 30,
            borderTopRightRadius: 30,
          }}>
          <View style={{flexDirection: 'row', marginBottom: 30}}>
            <View
              style={{
                backgroundColor: '#FFFFFF',
                borderRadius: 9,
                elevation: 5,
                padding: 16,
                flex: 1,
                marginRight: 4,
              }}>
              <Text
                style={{
                  color: '#000000',
                  fontSize: 30,
                  fontFamily: 'SourceSansPro-Bold',
                }}>
                5129
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  color: '#656565',
                  fontFamily: 'SourceSansPro-SemiBold',
                }}>
                Sales Today
              </Text>
            </View>

            <View
              style={{
                backgroundColor: '#FFFFFF',
                borderRadius: 9,
                elevation: 5,
                padding: 16,
                flex: 1,
                marginLeft: 4,
              }}>
              <Text
                style={{
                  color: '#000000',
                  fontSize: 30,
                  fontFamily: 'SourceSansPro-Bold',
                }}>
                23
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  color: '#656565',
                  fontFamily: 'SourceSansPro-SemiBold',
                }}>
                Unit Sold
              </Text>
            </View>
          </View>

          <View
            style={{
              backgroundColor: 'rgba(143, 207, 255, .32)',
              borderRadius: 20,
              padding: 16,
              marginBottom: 30,
              elevation: 0.2,
            }}>
            <View style={{marginBottom: 20, width: '100%'}}>
              <View style={{width: '100%', flex: 1}}>
                <Text
                  style={{
                    fontSize: 16,
                    color: '#1D1D1D',
                    fontFamily: 'SourceSansPro-Bold',
                    flex: 1,
                  }}>
                  Product Sales
                </Text>
              </View>
              <View>
                <DropDownPicker
                  items={[
                    {label: 'Last Week', value: 'uk'},
                    {label: 'Last Six Month', value: 'france'},
                  ]}
                  defaultValue={country}
                  containerStyle={{height: 40}}
                  style={{backgroundColor: '#fafafa', width: '100%'}}
                  dropDownStyle={{backgroundColor: '#fafafa'}}
                  onChangeItem={(item) => handleData(item)}
                />
              </View>
            </View>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 2}}>
                <Text
                  style={{
                    fontSize: 24,
                    color: '#656565',
                    fontFamily: 'SourceSansPro-Bold',
                  }}>
                  32.2k
                </Text>
                <Text
                  style={{
                    fontSize: 10,
                    color: '#656565',
                    fontFamily: 'SourceSansPro-Regular',
                  }}>
                  Last Week
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: 'rgba(249, 71, 71, .32)',
                  borderRadius: 10,
                  padding: 10,
                  marginRight: 30,
                  flex: 1,
                }}>
                <Text
                  style={{
                    fontSize: 16,
                    color: '#CF0000',
                    fontFamily: 'SourceSansPro-SemiBold',
                  }}>
                  31%
                </Text>
                <Text
                  style={{
                    fontSize: 10,
                    color: '#CF0000',
                    fontFamily: 'SourceSansPro-Regular',
                  }}>
                  Prev Week
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: 'rgba(34, 175, 0, .32)',
                  borderRadius: 10,
                  padding: 10,
                  flex: 1,
                }}>
                <Text
                  style={{
                    fontSize: 16,
                    color: '#0C7A00',
                    fontFamily: 'SourceSansPro-SemiBold',
                  }}>
                  31%
                </Text>
                <Text
                  style={{
                    fontSize: 10,
                    color: '#0C7A00',
                    fontFamily: 'SourceSansPro-Regular',
                  }}>
                  Overall
                </Text>
              </View>
            </View>
            <View>
              <BarChart
                style={{height: 200}}
                data={data}
                svg={{fill}}
                contentInset={{top: 30, bottom: 30}}
                spacingOuter={0.1}
                spacingInner={0.7}
              />
            </View>
          </View>

          <View
            style={{
              backgroundColor: '#FFFFFF',
              borderRadius: 20,
              padding: 16,
              elevation: 5,
            }}>
            <Text
              style={{
                fontSize: 16,
                color: '#1D1D1D',
                fontFamily: 'SourceSansPro-SemiBold',
                marginBottom: 22,
              }}>
              Lorem ipsum demo
            </Text>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                style={{
                  backgroundColor: '#0091FF',
                  borderRadius: 12,
                  padding: 10,
                  flex: 1,
                  marginRight: 6,
                }}>
                <Text
                  style={{
                    fontSize: 14,
                    color: '#FFFFFF',
                    fontFamily: 'SourceSansPro-SemiBold',
                  }}>
                  Add
                </Text>
                <View
                  style={{
                    alignItems: 'flex-end',
                    marginTop: 5,
                  }}>
                  <Image
                    style={{
                      width: 45,
                      height: 45,
                    }}
                    resizeMode={'center'}
                    source={require('../Asset/img/gift.png')}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: '#0091FF',
                  borderRadius: 12,
                  padding: 10,
                  flex: 1,
                  marginHorizontal: 6,
                }}>
                <Text
                  style={{
                    fontSize: 14,
                    color: '#FFFFFF',
                    fontFamily: 'SourceSansPro-SemiBold',
                  }}>
                  Sell
                </Text>
                <View
                  style={{
                    alignItems: 'flex-end',
                    marginTop: 5,
                  }}>
                  <Image
                    style={{
                      width: 45,
                      height: 45,
                    }}
                    resizeMode={'center'}
                    source={require('../Asset/img/gift.png')}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: '#0091FF',
                  borderRadius: 12,
                  padding: 10,
                  flex: 1,
                  marginLeft: 6,
                }}>
                <Text
                  style={{
                    fontSize: 14,
                    color: '#FFFFFF',
                    fontFamily: 'SourceSansPro-SemiBold',
                  }}>
                  Refer
                </Text>
                <View
                  style={{
                    alignItems: 'flex-end',
                    marginTop: 5,
                  }}>
                  <Image
                    style={{
                      width: 45,
                      height: 45,
                    }}
                    resizeMode={'center'}
                    source={require('../Asset/img/gift.png')}
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default App;
