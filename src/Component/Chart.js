import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {BarChart, Grid} from 'react-native-svg-charts';

const Chart = () => {
  const [data, setdata] = useState([10, 15, 20, 14, 8, 5]);
  const fill = 'rgba(1, 101, 177, 0.5)';

  return (
    <View>
      <BarChart
        style={{height: 200}}
        data={data}
        svg={{fill}}
        contentInset={{top: 30, bottom: 30}}
        spacingOuter={0.1}
        spacingInner={0.7}
      />
    </View>
  );
};

export default Chart;
