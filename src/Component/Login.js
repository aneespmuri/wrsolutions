import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {ScrollView} from 'react-native-gesture-handler';

var {width} = Dimensions.get('window');

const App = ({navigation}) => {
  const [username, setusername] = useState('');
  const [password, setpassword] = useState('');
  const [secure, setsecure] = useState(true);

  const findLogin = () => {
    if (username === 'admin' && password === '123456') {
      navigation.navigate('Drawesa');
    } else {
      alert('check');
    }
  };

  return (
    <ScrollView>
      <View style={{flex: 1}}>
        <LinearGradient colors={['#0091FF', '#004980']}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              // padding: 16,
              paddingTop: 30,
            }}>
            <Image
              style={{width: '100%', height: 100, marginBottom: 30}}
              resizeMode={'center'}
              source={require('../Asset/img/WRSolutions.png')}
            />
            <Text
              style={{
                fontSize: 20,
                color: '#FBFBFB',
                fontFamily: 'SourceSansPro-Regular',
                marginBottom: 20,
              }}>
              Sign In
            </Text>
          </View>
        </LinearGradient>

        <View style={{padding: 16}}>
          <View style={{marginBottom: 25}}>
            <Text
              style={{
                fontSize: 16,
                color: '#ABABAB',
                fontFamily: 'SourceSansPro-Regular',
              }}>
              Username
            </Text>
            <TextInput
              onChangeText={setusername}
              placeholder="Username"
              placeholderTextColor="#535353"
              style={{borderBottomColor: '#43ABFB', borderBottomWidth: 2}}
            />
          </View>
          <Text
            style={{
              fontSize: 16,
              color: '#ABABAB',
              fontFamily: 'SourceSansPro-Regular',
            }}>
            Password
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <TextInput
              secureTextEntry={secure}
              onChangeText={setpassword}
              placeholder="Password"
              placeholderTextColor="#535353"
              style={{
                borderBottomColor: '#43ABFB',
                borderBottomWidth: 2,
                flex: 1,
              }}
            />
            <TouchableOpacity onPress={() => setsecure(!secure)}>
              <Image
                style={{
                  width: 20,
                  height: 20,
                }}
                source={require('../Asset/img/eye.png')}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={{
              marginTop: 35,
              alignItems: 'flex-end',
              width: 'auto',
            }}>
            <Text
              style={{
                color: '#535353',
                fontFamily: 'SourceSansPro-SemiBold',
                fontSize: 17,
              }}>
              Forgot Password ?
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={findLogin}
            style={{
              backgroundColor: '#43ABFB',
              borderRadius: 34,
              padding: 10,
              alignItems: 'center',
              marginTop: 50,
            }}>
            <Text
              style={{
                fontSize: 22,
                fontFamily: 'SourceSansPro-SemiBold',
                color: '#FEFEFE',
              }}>
              Sign In
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default App;

const styles = StyleSheet.create({});
